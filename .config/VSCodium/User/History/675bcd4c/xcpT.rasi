/**
 * Nordic rofi theme
 * Adapted by undiabler <undiabler@gmail.com>
 *
 * Nord Color palette imported from https://www.nordtheme.com/
 *
 */

configuration {

	font: "Envy Code R 10";
    // font: "Font Awesome 6 Free"; // finner ikke fonten 

	line-margin: 50;
	lines: 10;

    display-ssh:    "SSH";
    display-run:    "RUN";
    display-drun:   "DRUN";
    display-window: "WINDOW";
    display-combi:  "COMBI";
    display-emoji:  "EMOJI";
    show-icons:     true;
}

* {
	nord0: #2e3440;
	nord1: #3b4252;
	nord2: #434c5e;
	nord3: #4c566a;

	nord4: #d8dee9;
	nord5: #e5e9f0;
	nord6: #eceff4;

	nord7: #8fbcbb;
	nord8: #88c0d0;
	nord9: #81a1c1;
	nord10: #5e81ac;
	nord11: #bf616a;

	nord12: #d08770;
	nord13: #ebcb8b;
	nord14: #a3be8c;
	nord15: #b48ead;

    bg-color:  rgba(46,52,64,80 %);
    bg: argb:f0465264;
    bg2: argb:40465264;
    highlight: underline bold #eceff4;

    background-color: rgba(46,52,64,0 %);
    transparent: rgba(0,0,0,0 %);

    transparency: "real";
}

window {
    location: center;
    anchor:   center;
    fullscreen: true;
    padding: 300 500;
    spacing: 0;
    children:  [mainbox];
    orientation: horizontal;
    background-color: @bg;
}

window.box {
    background-color: @nord15;
    background-color: @transparent;
}

mainbox {
    spacing: 0px;
    children: [ inputbar, mode-switcher, message, listview ];
    background-color: @transparent;
}

message {
    color: @nord0;
    padding: 5;
    border:  0px 0px 0px 0px;
}

wrapper-mode-switcher {
    orientation: horizontal;
    expand:     false;
    spacing:    0;
    children: [mode-switcher];
    background-color: @transparent;
}

inputbar {
    padding: 11px;
    color: @nord9;
    background-color: @transparent;
}

entry, prompt, case-indicator {
    margin: 0px 0.3em 0em 0em ;
    text-font: inherit;
    text-color:inherit;
    background-color: @transparent;
}

mode-switcher { 
    expand: false;
    orientation: horizontal;
    spacing: 5px;
    padding: 5px;
    background-color: @transparent;
}

listview {
    padding: 25px;
    color: @nord10;
    background-color: @transparent;
}

element {
    padding: 7px;
    vertical-align: 0.5;
    text-color: rgb(216, 222, 233);
    background-color: @transparent;
}

element selected.normal {
	background-color: @nord7;
	text-color: #2e3440;
    background-color: @transparent;
}

element-text, element-icon {
    background-color: inherit;
    text-color:       inherit;
    background-color: @transparent;
}
element.normal.active {
    text-color:       @highlight;
    background-color: @transparent;
}

button {
    padding: 6px;
    color: @nord9;
    horizontal-align: 0.5;
    background-color: @transparent;
}

button selected normal {
    background-color: @transparent;
}
