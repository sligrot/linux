#!/usr/bin/env bash


stdConf="-theme themes/nord -config ~/.config/rofi/config"

optstring=":sec"
while getopts ${optstring} arg; do
    case ${arg} in
        c)
            rofi -theme themes/nord -modi calc -show calc -no-sort 
            ;;
        e)
            rofi -theme themes/nord -config ~/.config/rofi/config -show-icons -modi emoji -show emoji
            ;;
        s) 
            rofi -theme themes/nord -modi combi, window -show
            ;;
        *)
            printf "No arguments"
            ;;
    esac
done