#!/usr/bin/env bash


stdConf="-theme themes/nord -config ~/.config/rofi/config"

optstring=":secl"
while getopts ${optstring} arg; do
    case ${arg} in
        c)
            rofi -theme themes/nord -modi calc -show calc -no-sort 
            ;;
        e)
            rofi -theme themes/nord -config ~/.config/rofi/config -modi emoji -show emoji -columns 3
            ;;
        s)
            rofi -theme themes/nord -combi-modi drun,run,window -modi combi,drun,run,window -show
            ;;
        l)
            rofi -theme themes/nord -modi powermenu:/home/oxo/.config/rofi/scripts/rofi-power-menu.sh -show powermenu
            ;;
        *)
            rofi -theme themes/nord -modi combi -show
            ;;
    esac
done
