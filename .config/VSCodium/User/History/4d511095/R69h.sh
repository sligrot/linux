#!/usr/bin/env bash


stdConf="-theme themes/nord -config ~/.config/rofi/config"

optstring=":sec"
while getopts ${optstring} arg; do
    case ${arg} in
        c)
            rofi -modi calc -show calc -no-sort 
            ;;
        e)
            rofi -modi emoji -show emoji
            ;;
        s) 
            rofi -modi combi, window -show
            ;;
        *)
            printf "No arguments"
            ;;
    esac
done