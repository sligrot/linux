#!/bin/bash
# DEPENDANCY
# wl-clipboard, imvr, grim

# TODO
# - Block the image viewer opening multiple times
# - Use stdout instead of logfile
# - Check to see if there is a last screenshot, if so, don't open the viewer
# - Find the last file in screenshot directory instead of using a log file.
# 
# CUSTOM MODULE IN WAYBAR
#
#    "custom/snip": {
#        "format": "",
#        "interval": 10,
#        "on-click": "$HOME/.config/waybar/screenshot.sh 1",
#        "on-click-right": "$HOME/.config/waybar/screenshot.sh 2",
#        "on-click-middle": "$HOME/.config/waybar/screenshot.sh 3",
#        "tooltip": true
#    },

SCREENSHOT_DIR="$HOME/screenshots"
LOG_FILE="$SCREENSHOT_DIR/last-screenshot.log"
 
# create logfile
[ -f $LOG_FILE ] && touch $LOG_FILE

case $1 in
	1) grim -t png $SCREENSHOT_DIR'/'$(date +%Y-%m-%d_%H-%m-%S).png | echo $SCREENSHOT_DIR'/'$(date +%Y-%m-%d_%H-%m-%S).png > $LOG_FILE
		LAST_SCREENSHOT=$(cat $LOG_FILE);
		[ -f $LAST_SCREENSHOT ] && wl-copy < $LAST_SCREENSHOT ;; # put shot on clipboard
	2) FIELD="$(slurp)";
		grim -g "$FIELD" $SCREENSHOT_DIR'/'$(date +%Y-%m-%d_%H-%m-%S).png | echo $SCREENSHOT_DIR'/'$(date +%Y-%m-%d_%H-%m-%S).png > $LOG_FILE;
		LAST_SCREENSHOT=$(cat $LOG_FILE)
		[ -f $LAST_SCREENSHOT ] && wl-copy < $LAST_SCREENSHOT ;; # put shot on clipboard
	3) 	LAST_SCREENSHOT=$(cat $LOG_FILE)
		echo $LAST_SCREENSHOT | imvr ;;
esac
