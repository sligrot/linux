
# script for changing wallpaper

# Wallpaper
Folder=/home/oxo/.config/i3/wallpaper/
#TimeToShow=
bgColor="#101015"

echo "654656546564554" > /home/oxo/.config/i3/scripts/loggwallpaper.log

setWallpaper() {
# check if feh is present ||(or) check if folder var i set &&(continue) set background with "feh"
[ ! -z $(which feh) ] || [ ! -z $Folder ] && $(which feh) --randomize --bg-fill $Folder
}

setColor() {
xsetroot -solid "$bgColor" &
}

# if setwallpaper fails try setcolor
setWallpaper || setColor || exit
