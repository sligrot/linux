
*******************************Release Note******************************************

This Package is for R9M MINI/MM-OTA firmware update. 

Version and Files: 
R9MINI_OTA_LBT.frsk                                 LBT (certificate for CE )Firmware for R9M MINI/MM-OTA. 
R9MINI_OTA_FCC.frsk                                Non-LBT (certificate for FCC )Firmware for R9M MINI/MM-OTA. 
R9MINI_OTA_FLEX.frsk                               FLEX Firmware(No certificate) for R9M MINI/MM-OTA.

readme.txt                                                    Release note 

F.port / S.Port can be switched in the radio menu.

Firmware Version: v1.3.1

The release firmware resolved the issues below:
--------------------------------------------------------------------------------------------------------------------
1. Fixed interference with multiple modules and receivers working together.
-------------------------------------------------------------------------------------------------------------------
How to update R9M MINI/MM-OTA firmware :
By radio (SD card) :
1. Put the firmware under the folder [FIRMWARE] of sd card.
2. Connect receiver S.Port to the S.Port of radio.
3. Power on the radio and find the firmware,select it by press [ENT].
4. Select Flash xxx(receiver name), wait to end.
---------------------------------------------------------------------------------------------------------------------
More details please check FrSky website:
https://www.frsky-rc.com/product/r9-mm-ota/
https://www.frsky-rc.com/product/r9-mini-ota/

**********************All rights reserved to FrSky Electronic ., Ltd.*********************************
