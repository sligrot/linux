
*******************************Release Note******************************************

This Package is for R9M 2019 firmware update. 

Version and Files: 
R9M2019_LBT.frk                                                LBT (certificate for CE )Firmware for R9M 2019.
R9M2019_FCC.frk                                               Non-LBT (certificate for FCC )Firmware for R9M 2019. 
R9M2019_FLEX.frk                                              FLEX Firmware(No certificate) for R9M 2019.
readme.txt                                                           Release note 
 
Firmware Version: v1.3.0

The release firmware resolved the issues below:
--------------------------------------------------------------------------------------------------------------------
1. Improve the RF performance.
2. Add telemetry of LFR(Lost Frame Rate) .
3. Add telemetry of power value under self power adaptive mode.
-------------------------------------------------------------------------------------------------------------------
How to update R9M 2019 firmware :
By radio (SD card) :
1. Put the firmware under the folder [FIRMWARE] of sd card.
2. Plug in R9M Lite 2019 into the module bay of the radio.
3. Power on the radio and find the firmware,select it by press [ENT].
4. Select 'Flash ext. module', wait to end.
---------------------------------------------------------------------------------------------------------------------
More details please check FrSky website:
https://www.frsky-rc.com/product/r9m-lite/

**********************All rights reserved to FrSky Electronic ., Ltd.*********************************
